﻿using System;

public enum CardSuit
{
    Hearts,
    Clubs,
    Diamonds,
    Spades
}
